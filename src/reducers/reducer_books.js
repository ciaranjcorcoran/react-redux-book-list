export default function() {
  return [
    { title: 'Javascript: The Good Parts', pages: 101 },
    { title: 'Sapiens', pages: 400 },
    { title: 'Born to Run', pages: 200 },
    { title: 'Bird Box', pages: 300 }
  ];
}